﻿namespace IS_lab6
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.chooseEncoding = new System.Windows.Forms.ComboBox();
            this.chooseFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.crcText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.calculate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chooseEncoding
            // 
            this.chooseEncoding.FormattingEnabled = true;
            this.chooseEncoding.Items.AddRange(new object[] {
            "UTF-32",
            "UTF-7",
            "UTF-8",
            "UTF-16",
            "ASCII",
            "Windows-1251",
            "Windows-1252",
            "ISO-8859-1"});
            this.chooseEncoding.Location = new System.Drawing.Point(101, 48);
            this.chooseEncoding.Name = "chooseEncoding";
            this.chooseEncoding.Size = new System.Drawing.Size(211, 21);
            this.chooseEncoding.TabIndex = 0;
            // 
            // chooseFile
            // 
            this.chooseFile.Location = new System.Drawing.Point(101, 12);
            this.chooseFile.Name = "chooseFile";
            this.chooseFile.Size = new System.Drawing.Size(211, 23);
            this.chooseFile.TabIndex = 1;
            this.chooseFile.Text = "...";
            this.chooseFile.UseVisualStyleBackColor = true;
            this.chooseFile.Click += new System.EventHandler(this.chooseFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Файл";
            // 
            // crcText
            // 
            this.crcText.Enabled = false;
            this.crcText.Location = new System.Drawing.Point(101, 82);
            this.crcText.Name = "crcText";
            this.crcText.Size = new System.Drawing.Size(211, 20);
            this.crcText.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "CRC-32";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Кодировка";
            // 
            // calculate
            // 
            this.calculate.Location = new System.Drawing.Point(23, 117);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(289, 23);
            this.calculate.TabIndex = 6;
            this.calculate.Text = "Вычислить";
            this.calculate.UseVisualStyleBackColor = true;
            this.calculate.Click += new System.EventHandler(this.calculate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 148);
            this.Controls.Add(this.calculate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.crcText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chooseFile);
            this.Controls.Add(this.chooseEncoding);
            this.Name = "Form1";
            this.Text = "CRC-32";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox chooseEncoding;
        private System.Windows.Forms.Button chooseFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox crcText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button calculate;
    }
}

