﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace IS_lab6
{
    public partial class Form1 : Form
    {
        StreamReader sr;
        public Form1()
        {
            InitializeComponent();
        }

        private void chooseFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if(ofd.ShowDialog()==DialogResult.OK)
            {
                chooseFile.Text = ofd.SafeFileName;
                sr = new StreamReader(ofd.OpenFile(),Encoding.Default);
                calculate.Enabled = true;
            }
        }

        private void calculate_Click(object sender, EventArgs e)
        {
            if(chooseFile.Text=="...")
            {
                MessageBox.Show("Выберите файл.", "Не выбран файл!", MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
            string input = sr.ReadToEnd();
            crcText.Text = crc(input, chooseEncoding.Items[chooseEncoding.SelectedIndex].ToString()).ToString("X2");
        }

        private UInt32 crc(string input, string encoding)
        {
            byte[] source = Encoding.GetEncoding(encoding).GetBytes(input);

            UInt32[] crc_table = new UInt32[256];
            UInt32 crc;

            for (UInt32 i = 0; i < 256; i++)
            {
                crc = i;
                for (UInt32 j = 0; j < 8; j++)
                    crc = (crc & 1) != 0 ? (crc >> 1) ^ 0xEDB88320 : crc >> 1;

                crc_table[i] = crc;
            };

            crc = 0xFFFFFFFF;

            foreach (byte s in source)
            {
                crc = crc_table[(crc ^ s) & 0xFF] ^ (crc >> 8);
            }

            crc ^= 0xFFFFFFFF;
            return crc;
        }
    }
}
